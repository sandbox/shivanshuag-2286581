<?php

/**
 * @file
 * Contains Drupal\auth_bug\EventSubscriber\AuthBugSubscriber.
 */

namespace Drupal\auth_bug\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Response;
use \Drupal\Component\Utility\String;

/**
 * Subscribes to the kernel request event to check whether authentication is required
 */
class AuthBugSubscriber implements EventSubscriberInterface {

  public function onKernelRequest(GetResponseEvent $event) {
    $request = $event->getRequest();
    if (!($request->headers->get('PHP_AUTH_USER') != null) && !($request->headers->get('PHP_AUTH_PW') != null)) {
      $response =  new Response();
      $response->setStatusCode(401);
      $site_name = \Drupal::config('system.site')->get('name');
      global $base_url;
      $challenge = String::format('Basic realm="@realm"', array(
        '@realm' => !empty($site_name) ? $site_name : $base_url,
      ));

      $response->headers->set('WWW-Authenticate', $challenge);
      $response->send();
    }

  }
  static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('onKernelRequest', 0);
    return $events;
  }


}
